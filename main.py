from copy import deepcopy

class Factor:
    vars = []
    probabilities = []

    def __init__(self, var):
        if var is None:
            self.vars = []
            self.probabilities = [({}, 1)]
        else:
            self.vars = []
            self.probabilities = []
            self.vars = var.get_parents()
            self.vars.append(var.get_name())
            for probability in var.get_probabilities():
                probability_1 = deepcopy(probability)
                probability_0 = deepcopy(probability)

                probability_1[0][var.get_name()] = 1
                probability_0[0][var.get_name()] = 0
                probability_0 = (probability_0[0], 1 - probability_0[1])

                self.probabilities.append(probability_1)
                self.probabilities.append(probability_0)


    def multiply(self, factor):
        all_vars = list(set(self.vars).union(set(factor.get_vars())))
        common_vars = list(set(self.vars) & set(factor.get_vars()))

        new_probabilities = []

        for prob_1 in self.probabilities:
            for prob_2 in factor.get_probabilities():
                ok = True
                for var in common_vars:
                    if prob_1[0][var] != prob_2[0][var]:
                        ok = False
                if ok:
                    new_vars_values = prob_1[0].copy()
                    new_vars_values.update(prob_2[0])
                    new_prob_value = prob_1[1] * prob_2[1]
                    new_probabilities.append((new_vars_values, new_prob_value))

        self.vars = all_vars
        self.probabilities = new_probabilities

    def get_vars(self):
        return self.vars

    def get_probabilities(self):
        return self.probabilities

    def __str__(self):
        return "Factor " + "".join(self.vars) + " " + str(self.vars) + " | " + str(len(self.probabilities)) + " probabilitati -> " + " " + str(self.probabilities)


# Definim o variabila care poate aparea in graful nostru
class Var:
    name = None
    parents = []
    probabilities = []

    neighbours = []

    # Incrementam un tuplu de forma (x,x,x,x)
    def inc_truth_values(self, truth_values):
        for i in range(len(truth_values)-1, -1, -1):
            if truth_values[i] is 1:
                truth_values[i] = 0
            else:
                truth_values[i] = 1
                break
        return truth_values


    # var_def = lista cu [nume, [parinti], [valori prob]]
    def __init__(self, var_def):
        self.name = var_def[0]
        self.parents = var_def[1]
        self.probabilities = []
        self.neighbours = []

        # Asociem valorile de adevar posibile pentru parinti cu valorile prob
        truth_values = [0] * len(self.parents)
        for probability in var_def[2]:
            dictionary = dict(zip(self.parents, truth_values))
            self.probabilities.append((dictionary, probability))
            self.inc_truth_values(truth_values)

    def add_neighbour(self, neighbour_name):
        self.neighbours.append(neighbour_name)

    def get_name(self):
        return self.name

    def get_parents(self):
        return self.parents

    def get_probabilities(self):
        return self.probabilities

    def get_neighbours(self):
        return self.neighbours

    def __str__(self):
        result = self.name + " | " + str(self.parents) + " | " + str(self.neighbours) + " | " + str(self.probabilities) + "\n"
        return result

class Clique:
    # Numele variabilelor din care este compusa clica
    vars = []
    # Clicile cu care se invecineaza
    neighbours = {}
    # Factorul
    factor = None
    # Parintele in arborele de clici
    parent = None
    # Copiii din arborele de clici
    children = []

    def __init__(self, vars):
        self.vars = vars
        self.neighbours = {}
        self.factor = Factor(None)
        self.parent = None
        self.children = []

    def add_neighbour(self, clique):
        # Adauga clica in lista de vecini daca se invecineaza cu cea curenta
        number_of_neighbours = self.clique_edge_cost(clique)
        if number_of_neighbours > 0:
            self.neighbours[clique] = number_of_neighbours

    def clique_edge_cost(self, clique):
        # Numaram cate variabile din clica curenta se afla in clica vecina
        return len(self.common_vars(clique))

    def common_vars(self, clique):
        return list(set(self.vars) & set(clique.get_vars()))

    def insert_factor(self, f):
        if set(f.get_vars()).issubset(set(self.vars)):
            self.factor.multiply(f)
            return True
        return False

    def get_vars(self):
        return self.vars

    def get_neighbours(self):
        return self.neighbours

    def get_factor(self):
        return self.factor

    def get_name(self):
        return "".join(self.vars)

    def set_parent(self, parent):
        self.parent = parent

    def get_parent(self):
        return self.parent

    def add_child(self, child):
        self.children.append(child)

    def get_children(self):
        return self.children

    def __str__(self):
        result = self.get_name() + " -> " + str(len(self.neighbours)) + " vecini {";
        for c in self.neighbours:
            result += c.get_name() + ": " + "".join(self.common_vars(c)) + " (" + str(self.neighbours[c]) + ") | "
        result += "}"
        return result


# Construim acest graf orientat G initial
class G:
    input_file = None
    variables_number = 0
    inferences_number = 0

    variables = []
    chordal_vars = []

    cliques = []
    cliques_tree = []

    factors = []

    clique_queen = None

    def __init__(self, input_file):
        self.input_file = input_file
        self.read_graph()

    # FACEM MARE PARSARE
    def parse_var_line(self, line):
        # Spargem linia in cele 3 componenete
        line = line.split(";")
        # Scoatem niste " " inutile citite de pe linie
        line[0] = line[0][:-1]
        line[1] = line[1][1:-1]
        line[2] = line[2][1:-1]
        # Spargem frumos listele ca sa liste cu parinti / probabiltati
        line[1] = line[1].split(" ")
        line[2] = line[2].split(" ")
        # Obtinem valorile numerice pentru probabilitati
        line[2] = list(map(lambda value: float(value), line[2]))

        if line[1] == ['']:
            line[1] = []

        return line

    def read_graph(self):
        # Deschidem fisierul pentru ca urmaeaza o mare citire de graf
        self.file = open(self.input_file, "r")
        # Citim numarul de variabile si de inferente
        v, i = self.file.readline().split(" ")
        # Le salvam intern
        self.variables_number = int(v)
        self.inferences_number = int(i)

        # Citim configuratiile pentru fiecare variabile
        for i in range(self.variables_number):
            line = self.file.readline()
            # Procesam putin linia citita
            line = self.parse_var_line(line)
            # Ne definim o noua variabila pe care sa o adaugam in graf
            self.variables.append(Var(line))

    # TASK 1.1
    # Vom transforma arborele dintr-unul orientat intr-unul neorientat
    # Pentru aceasta fiecare variabila isi va nota vecinii (parinti sau copii)
    def transform_graph_U(self):
        for var in self.variables:
            for parent_var_name in var.get_parents():
                # Variabila si parintele ei vor deveni vecini
                self.add_edge(var.get_name(), parent_var_name)

    # TASK 1.2
    # Moralizam graful, adica il aducem pe calea cea buna
    # Parintii variabilelor se vor invecina 2 cate 2
    def transform_graph_H(self):
        for var in self.variables:
            for i in range(len(var.get_parents())):
                parent_1_name = var.get_parents()[i]
                # Evitam sa luam (X,Y) si (Y,X)
                for j in range(i+1, len(var.get_parents())):
                    parent_2_name = var.get_parents()[j]
                    self.add_edge(parent_1_name, parent_2_name)

    # Task 1.3
    # Cream graful cordal calculand la fiecare pas costurile pana cand
    # nu mai avem ce noduri sa eliminam

    def transform_graph_H_star(self):
        # Salvam numele variabilelor pentru a face eliminarile
        self.chordal_vars = list(map(lambda var: var.get_name(), self.variables))
        # Cat timp putem elimina variable, o facem
        while True:
            # Calculam costurile pentru variabilele ramase
            costs = self.calculate_chordal_costs()
            # Ne vom opri cand toti vecinii nodurilor sunt conectati
            if self.all_costs_zero(costs):
                break
            # Eliminam variabilele cu cost 0
            # In cazul in care am gasit astfel de variabile, avem nevoie de o noua iteratie
            found_zeros = False
            for var_name in costs:
                if costs[var_name] == 0:
                    self.chordal_vars.remove(var_name)
                    found_zeros = True
            if found_zeros:
                continue

            # Alegem la intamplare o variabila cu cost minim
            best_var = min(costs, key=costs.get)
            self.add_missing_edges(best_var)
            self.chordal_vars.remove(best_var)

    # Verificam daca toate costurile obtinute sunt 0
    # In cazul in care sunt, nu vom mai elimina noduri din graf
    def all_costs_zero(self, costs):
        for var_name in costs:
            if costs[var_name] != 0:
                return False
        return True

    # Calculam costurile pentru fiecare variabila din graf
    def calculate_chordal_costs(self):
        return {
            var_name: self.calculate_chordal_cost_for_var(var_name)
            for var_name in self.chordal_vars
        }

    # Calculam costul unei variabile in graful cordal
    def calculate_chordal_cost_for_var(self, var_name):
        # Obtinem lista de vecini a variabilei, eliminand vecinii care au fost scosi
        neighbours = self.get_variable(var_name).get_neighbours()
        # Intersectam vecinii variabilei cu variabilele inca existente in graf
        neighbours = list(set(neighbours) & set(self.chordal_vars))
        # Verificam cati vecini ai variabilei nu se invecineaza inca
        cost = 0
        n_parents = len(neighbours)
        for i in range(n_parents):
            for j in range(i+1, n_parents):
                # Evitam sa verificam si (X, Y) si (Y, X)
                neighbour_1 = neighbours[i]
                neighbour_2 = neighbours[j]
                # Daca vecinii nu se invecineaza, marim costul
                if not self.exists_edge(neighbour_1, neighbour_2):
                    cost += 1
        return cost

    # Adaugam muchiile lipsa intre vecinii unei variabile
    def add_missing_edges(self, var_name):
        neighbours = self.get_variable(var_name).get_neighbours()
        neighbours = list(set(neighbours) & set(self.chordal_vars))
        n_parents = len(neighbours)
        for i in range(n_parents):
            for j in range(i+1, n_parents):
                # Evitam sa cuplam si (X, Y) si (Y, X)
                neighbour_1 = neighbours[i]
                neighbour_2 = neighbours[j]
                # Daca vecinii nu se invecineaza, adaugam o muchie intre ei
                if not self.exists_edge(neighbour_1, neighbour_2):
                    self.add_edge(neighbour_1, neighbour_2)

    # Verificam daca intre nodurile X si Y avem muchie in graful neorientat
    def exists_edge(self, var_1_name, var_2_name):
        var_1 = self.get_variable(var_1_name)
        var_2 = self.get_variable(var_2_name)

        return (
            var_2_name in var_1.get_neighbours() and
            var_1_name in var_2.get_neighbours()
        )

    # Cream o muchie intre cele 2 variabile
    def add_edge(self, var_1_name, var_2_name):
        var_1 = self.get_variable(var_1_name)
        var_2 = self.get_variable(var_2_name)

        var_1.add_neighbour(var_2_name)
        var_2.add_neighbour(var_1_name)


    # Task 1.4
    # Aplicam CabronKerbosch pentru a obtine clicile maxime (copiem pseudocodul adica)
    def CabronKerbosch(self, R, P, X):
        # Facem cate o copie locala pentru fiecare lista de noduri
        # Asta ca sa nu stricam dreaqu ceva
        local_R = deepcopy(R)
        local_P = deepcopy(P)
        local_X = deepcopy(X)
        # Daca nu mai avem variabile Posibile si nu avem variabile Excluse, am gasit clica
        if local_P == [] and local_X == []:
            self.add_clique(local_R)
        # Cautam clici prin recursivitate
        for var_name in list(local_P):
            var_neighbours = self.get_variable(var_name).get_neighbours()
            self.CabronKerbosch(
                list(set(local_R).union(set(var_name))),
                list(set(local_P) & set(var_neighbours)),
                list(set(local_X) & set(var_neighbours))
            )
            local_P.remove(var_name)
            local_X.append(var_name)

    def get_cliques_graph_edges(self):
        cliques_graph = []
        for clique in self.cliques:
            for neighbour in clique.get_neighbours():
                cost = clique.get_neighbours()[neighbour]
                cliques_graph.append((clique, neighbour, cost))
        return cliques_graph

    # Adaugam o noua clica in graful de clici
    def add_clique(self, vars):
        clique = Clique(vars)
        for c in self.cliques:
            clique.add_neighbour(c)
            c.add_neighbour(clique)
        self.cliques.append(clique)

    # Task 1.5
    # Implementam Kruskal conform pseudocodului de pe Wikipedia
    def Kruskal(self):
        i = 0 # Indexul muchiei pe care o vom verifica
        e = 0 # Numarul de muchii gasite

        # Obtinem graful sub forma unei liste de tupluri (var1, var2, cost)
        cliques_graph = self.get_cliques_graph_edges()
        # Sortam descrescator muchiile dupa cost
        cliques_graph = list(reversed(sorted(cliques_graph, key=lambda edge: edge[2])))

        # Cream seturile pentru fiecare clica
        sets = [[clique] for clique in self.cliques]

        # Va trebui sa adaugam len(cliques) - 1 muchii in arbore
        while e < len(self.cliques) - 1:
            # Luam muchia cu cel mai mare cost
            (clique_1, clique_2, cost) =  cliques_graph[i]
            i += 1

            # Obtinem seturile din care fac parte cele 2 clici
            set_1 = None
            set_2 = None
            for s in sets:
                if clique_1 in s:
                    set_1 = s
                if clique_2 in s:
                    set_2 = s

            # Daca cele 2 seturi sunt diferite, putem adauga o muchie (altfel aveam ciclu)
            if set_1 != set_2:
                # Incrementam numarul de muhcii gasite
                e = e + 1
                # Adaugam muchia in arborele final
                self.cliques_tree.append((clique_1, clique_2, cost))
                # Unim cele 2 seturi ale clicilor (deoarece am adaugat o muchie)
                sets.remove(set_1)
                sets.remove(set_2)
                sets.append(list(set(set_1).union(set(set_2))))

    # Task 1.6
    # Calculam factorii initiali
    def set_initial_factors(self):
        for var in self.variables:
            self.factors.append(Factor(var))

        for factor in self.factors:
            for clique in self.cliques:
                # Daca am adaugat facotrul in clica (s-a potrivit), nu il vom adauga si in altele
                if clique.insert_factor(factor):
                    break

    # Task 2.1
    # Alegem o radacina pentru arborele de clici si stabilim relatiile parinte-copil
    def choose_cliques_queen(self):
        # Selectam random o clica care sa devina radacina arborelui
        self.clique_queen = self.cliques[0]
        self.clique_queen.set_parent(None)

        # Facem o parcurgere pe arbore
        visited_cliques = [self.clique_queen]
        queue = [self.clique_queen]
        while queue != []:
            # Parcurgem urmatoarea clica din coada
            current_clique = queue[0]
            visited_cliques.append(current_clique)
            # Cream o legatura parinte - copil intre clicile nevizitate catre care avem o muchie in arbore
            for clique in current_clique.get_neighbours():
                if clique not in visited_cliques and self.are_connected_cliques(current_clique, clique):
                    current_clique.add_child(clique)
                    clique.set_parent(current_clique)
                    queue.append(clique)
            # Eliminam muchia vizitata din coada
            queue.remove(current_clique)

    def are_connected_cliques(self, clique_1, clique_2):
        for edge in self.cliques_tree:
            if (edge[0] == clique_1 and edge[1] == clique_2) or (edge[0] == clique_2 and edge[1] == clique_1):
                return True
        return False

    # Obtinem o variabila identificata dupa nume
    def get_variable(self, var_name):
        for var in self.variables:
            if var.get_name() == var_name:
                return var
        return None

    def print_cliques(self):
        result = "-=-=-=-=-=-=-=-= Graful de clici -=-=-=-=-=-=-=-=" + "\n"
        result = "-=" * (len(result) / 2) + "\n" + result + "-=" * (len(result) / 2) + "\n"
        print result
        print "Am gasit " + str(len(self.cliques)) + " clici"
        for clique in self.cliques:
            print clique

    def print_cliques_tree(self):
        result = "-=-=-=-=-=-=-=-= Arborele de cost maxim de clici -=-=-=-=-=-=-=-=" + "\n"
        result = "\n" + "-=" * (len(result) / 2) + "\n" + result + "-=" * (len(result) / 2) + "\n"
        print result
        for (clique_1, clique_2, cost) in self.cliques_tree:
            print clique_1.get_name() + " " + clique_2.get_name() + " " + str(cost)

    def print_cliques_factors(self):
        result = "-=-=-=-=-=-=-=-= Factorii finali pentru clici -=-=-=-=-=-=-=-=" + "\n"
        result = "\n" + "-=" * (len(result) / 2) + "\n" + result + "-=" * (len(result) / 2) + "\n"
        print result
        for clique in self.cliques:
            print "Clica " + clique.get_name() + " -> " + str(clique.get_factor())

    def print_cliques_kingdom(self):
        result = "-=-=-=-=-=-=-=-= Printam arborele de clici (parinti - copii) -=-=-=-=-=-=-=-=" + "\n"
        result = "\n" + "-=" * (len(result) / 2) + "\n" + result + "-=" * (len(result) / 2) + "\n"
        for clique in self.cliques:
            if clique == self.clique_queen:
                print clique.get_name() + " -> None " + str(list(map(lambda c: c.get_name(), clique.get_children())))
            else:
                print clique.get_name() + " -> " + clique.get_parent().get_name() + " " + str(list(map(lambda c: c.get_name(), clique.get_children())))

    def set_title(self, title):
        self.title = title

    def __str__(self):
        result = "-=-=-=-=-=-=-=-= " + self.title + " -=-=-=-=-=-=-=-=" + "\n"
        result = "-=" * (len(result) / 2) + "\n" + result + "-=" * (len(result) / 2) + "\n"
        result += "Avem " + str(self.variables_number) + " variabile" + "\n"
        for var in self.variables:
            result += str(var)

        return result

def main():
    graph = G("bn1")
    graph.set_title("TASK 0 - Citirea grafului orientat G")
    print(graph)

    graph.set_title("TASK 1 - G -> U (graful neorientat)")
    graph.transform_graph_U()
    print(graph)

    graph.set_title("TASK 2 - U & G -> H (graful moralizat)")
    graph.transform_graph_H()
    print(graph)

    graph.set_title("TASK 3 - H -> H* (graful cordal)")
    graph.transform_graph_H_star()
    print(graph)

    # clici = mici pasari cu penaj colorat verde-albastru ce traiesc in zone insulare
    graph.set_title("TASK 4 - H* -> C (graful de clici)")
    R = []
    P = [var.get_name() for var in graph.variables]
    X = []
    graph.CabronKerbosch(R, P, X)
    graph.print_cliques()

    graph.set_title("TASK 5 - C -> T (arbore de acoperire de cost maxim)")
    graph.Kruskal()
    graph.print_cliques_tree()

    graph.set_title("TASK 6 - Adaugam factori in T")
    graph.set_initial_factors()
    graph.print_cliques_factors()

    print "\n\n\n"
    graph.set_title("TASK 7 - Relatii parinte-copil in arborele de clici")
    graph.choose_cliques_queen()
    graph.print_cliques_kingdom()


if __name__ == "__main__":
    main()
